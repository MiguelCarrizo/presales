class AddDataToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :name, :string
    add_column :users, :dni, :string
    add_column :users, :cuit, :string
    add_column :users, :address, :string
    add_column :users, :city, :string
    add_column :users, :zip_code, :string
    add_column :users, :telephone, :string
    add_column :users, :state, :string
    add_column :users, :type, :string
  end
end
